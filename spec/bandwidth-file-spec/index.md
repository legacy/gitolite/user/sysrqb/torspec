# Tor Bandwidth File Format

```text
                            juga
                            teor
```

This document describes the format of Tor's Bandwidth File, version
1.0.0 and later.

It is a new specification for the existing bandwidth file format,
which we call version 1.0.0. It also specifies new format versions
1.1.0 and later, which are backwards compatible with 1.0.0 parsers.

Since Tor version 0.2.4.12-alpha, the directory authorities use
the Bandwidth File file called "V3BandwidthsFile" generated by
Torflow \[1\]. The details of this format are described in Torflow's
README.spec.txt. We also summarise the format in this specification.
